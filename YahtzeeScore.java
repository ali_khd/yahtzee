/*
 * Problem Statement (200P):
 * This task is about the scoring in the first phase of the die-game Yahtzee, 
 * where five dice are used. The score is determined by the values on the 
 * upward die faces after a roll. The player gets to choose a value, and all 
 * dice that show the chosen value are considered active. The score is simply 
 * the sum of values on active dice.
 * Say, for instance, that a player ends up with the die faces showing 2, 2,
 * 3, 5 and 4. Choosing the value two makes the dice showing 2 active and 
 * yields a score of 2 + 2 = 4, while choosing 5 makes the one die showing 5 
 * active, yielding a score of 5.
 * Your method will take as input a int[] toss, where each element represents 
 * the upward face of a die, and return the maximum possible score with these 
 * values.
 * Constraints:
 * toss will contain exactly 5 elements.
 * Each element of toss will be between 1 and 6, inclusive.
 * Examples
 * 0) 
 * { 2, 2, 3, 5, 4 }
 * Returns: 5
 * The example from the text.
 * 1) 
 * { 6, 4, 1, 1, 3 }
 * Returns: 6
 * Selecting 1 as active yields 1 + 1 = 2, selecting 3 yields 3, selecting 4 
 * yields 4 and selecting 6 yields 6, which is the maximum number of points.
 * 2)  
 * { 5, 3, 5, 3, 3 }
 * Returns: 10
 */

//running time: O(1)
//memory: O(1)
public class YahtzeeScore{
	public static int maxPoints(int[] toss){
		//array count holds the active values 
		//count[1] is the sum of active values for 1
		int [] count = new int[6];    
		for (int i = 0; i < toss.length ; i++){
			count[toss[i] - 1] += toss[i];
		}

		//the answer is the maximum of active values stored in count
		int max = count[0];
		for (int i = 0; i < count.length ; i++){
			if (max < count[i]){
				max = count[i];
			}
			
		}
		return max;
	}
}